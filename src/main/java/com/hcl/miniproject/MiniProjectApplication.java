package com.hcl.miniproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.hcl.miniproject.model.User;
import com.hcl.miniproject.repository.UserRepositry;

@SpringBootApplication
public class MiniProjectApplication {

	@Autowired
	private UserRepositry userRepositry;
	public static void main(String[] args) {
		SpringApplication.run(MiniProjectApplication.class, args);
	}
	@Bean
	public void insertData() {

		User u1 = new User(1, "vns", "Nikil", "vnsNikhil@gmail.com", "nikhil123", "994534513", "india");
		User u2 = new User(2, "john", "wick", "John@gmail.com", "johnwick456", "899877634", "china");
		User u3 = new User(3, "obiWan", "kenobi", "hellothere@gmail.com", "obiWan678", "984563634", "sri lanka");
		User u4 = new User(4, "darth", "vader", "darkside@gmail.com", "98543", "9866587", "japan");
		User u5 = new User(5, "naruto", "uzumaki", "naruto@gmail.com", "654", "8756544", "england");
		
		userRepositry.save(u1);
		userRepositry.save(u2);
		userRepositry.save(u3);
		userRepositry.save(u4);
		userRepositry.save(u5);
	
	}

}
