package com.hcl.miniproject.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hcl.miniproject.model.User;
import com.hcl.miniproject.repository.UserRepositry;
import com.hcl.miniproject.services.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userBeanService;

	@GetMapping("/index")
	public String getIndex() {
		return "index";
	}

	@GetMapping("/login")
	public String getLogin(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "login";
	}

	@PostMapping("/login")
	public String postLogin(@RequestParam(required = false) String register, User userBean, HttpSession session) {
		if (register == null) {
			try {
				if (userBeanService.userValidation(userBean)) {
					session.setAttribute("name", userBeanService.getUserBean(userBean.getEmail()).getFirstname());
					session.setAttribute("email", userBeanService.getUserBean(userBean.getEmail()).getEmail());
					session.setAttribute("userId", userBeanService.getUserBean(userBean.getEmail()).getId());
					return "redirect:userdashboard";
				} else {

					return "redirect:login?error=Invalid creditionals";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block

				return "redirect:login?error=User not register!";

			}

		} else {
			return "redirect:register";
		}
	}

	@GetMapping("/register")
	public String getRegister(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "register";
	}

	@PostMapping("/register")
	public String userRegister(User userBean) {
		System.out.println(userBean);
		if (userBeanService.insertUser(userBean)) {
			return "redirect:login?error=You have successfuly rigister!";
		} else {
			return "redirect:register?error=You have already rigister!";
		}

	}

	@GetMapping("/logout")
	public String getLogout(HttpSession httpSession) {
		httpSession.removeAttribute("email");
		httpSession.removeAttribute("userId");
		httpSession.removeAttribute("name");
		return "index";
	}

	@Autowired
	public UserRepositry userRepositry;

	@GetMapping("/userdashboard")
	public String getuserDashboard(Map<String, List<User>> map) {
		System.out.println("*******===");
		map.put("contacts", (List<User>) userRepositry.findAll());
		System.out.println(map);
		return "userdashboard";
	}

	@GetMapping("/deleteuser")
	public String getDeleteUser(@RequestParam String id) {
		if (userBeanService.deleteUser(id)) {
			return "redirect:userdashboard";
		}
		return "redirect:userdashboard?error";
	}

	@GetMapping("/adduser")
	public String getUser(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "adduser";

	}

	@PostMapping("/adduser")
	public String addUsers(User userBean, HttpSession httpSession) {
		// contactBean.setUser(user1);
		try {
			if (userBeanService.addUser(userBean)) {
				return "redirect:userdashboard";
			} else {
				return "redirect:edit?error=You could not add contact!";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "redirect:edit?error=You could not add contact!";

		}
	}

	@GetMapping("/edituser")
	public String getEditContact(@RequestParam int id, Map<String, User> map) {
		User contactBean = userBeanService.getUser(id);
		System.out.println(contactBean);
		map.put("contact", contactBean);
		return "edituser";
	}

	@PostMapping("/edituser")
	public String postLogin(User contactBean, HttpSession httpSession) {
		try {
			if (userBeanService.updateContact(contactBean)) {

				return "redirect:userdashboard";
			} else {

				return "redirect:login?error=You could not edit!";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "redirect:edituser?error=You could not edit!";

		}
	}

	@GetMapping("/userlist")
	public String getBookList(Map<String, List<User>> map) {
		try {
			List<User> user = userBeanService.getAllBook();
			map.put("user", user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Unable to find the book lsit!");
			e.printStackTrace();
		}
		return "userlist";
	}

}
