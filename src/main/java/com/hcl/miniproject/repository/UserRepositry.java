package com.hcl.miniproject.repository;

import org.springframework.data.repository.CrudRepository;

import com.hcl.miniproject.model.User;

public interface UserRepositry extends CrudRepository<User, Integer> {
	public User findByEmail(String email);

	public boolean existsByEmail(String email);
}
