package com.hcl.miniproject.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.miniproject.model.User;
import com.hcl.miniproject.repository.UserRepositry;

@Service
public class UserService {
	@Autowired
	private UserRepositry userRepositry;

	public User getUserById(int userId) {
		return this.userRepositry.findById(userId).get();
	}

	public boolean insertUser(User user) {
		if (this.userRepositry.existsByEmail(user.getEmail())) {
			return false;
		}
		this.userRepositry.save(user);
		return true;
	}

	public User getUserBean(String email) throws Exception {
		return this.userRepositry.findByEmail(email);
	}

	public User getUser(int id) {
		return this.userRepositry.findById(id).get();
	}

	public boolean userValidation(User user) throws Exception {
		User existUser = null;
		try {
			existUser = getUserBean(user.getEmail());
			if (user.getPassword().equals(existUser.getPassword())) {
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		return false;
	}

	public boolean deleteUser(String id) {
		int a = Integer.parseInt(id);
		if (this.userRepositry.existsById(a)) {
			this.userRepositry.deleteById(a);
			return true;
		}
		return false;
	}

	public boolean addUser(User contactBean) {
		if (!this.userRepositry.existsById(contactBean.getId())) {
			this.userRepositry.save(contactBean);
			return true;
		}
		return false;
	}

	public boolean updateContact(User contactBean) {
		if (this.userRepositry.existsById(contactBean.getId())) {
			this.userRepositry.save(contactBean);
			return true;
		}
		return false;
	}

	public List<User> getAllBook() throws Exception {
		return (List<User>) this.userRepositry.findAll();
	}

}
