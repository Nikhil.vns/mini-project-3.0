<%@page import="com.graded.main.model.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>USERLIST</title>
<link rel="stylesheet" type="text/css" href="slide navbar style.css">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link
	href="https://fonts.googleapis.com/css2?family=Jost:wght@500&display=swap"
	rel="stylesheet">
<style type="text/css">
body {
	align-items: center;
	font-family: 'Jost', sans-serif;
	background: linear-gradient(to bottom, #0f0c29, #302b63, #24243e);
}

.main {
	width: 350px;
	height: 500px;
	background: red;
	overflow: hidden;
	background:
		url("https://doc-08-2c-docs.googleusercontent.com/docs/securesc/68c90smiglihng9534mvqmq1946dmis5/fo0picsp1nhiucmc0l25s29respgpr4j/1631524275000/03522360960922298374/03522360960922298374/1Sx0jhdpEpnNIydS4rnN4kHSJtU1EyWka?e=view&authuser=0&nonce=gcrocepgbb17m&user=03522360960922298374&hash=tfhgbs86ka6divo3llbvp93mg4csvb38")
		no-repeat center/cover;
	border-radius: 10px;
	box-shadow: 5px 20px 50px #000;
}

#h1 {
	color: white
}

p {
	color: white;
}

a {
	color: white;
}

.ae {
	color: white;
}

.col-4 {
	margin-bottom: 5%;
	padding-left: 2%;
}

h1 {
	color: white;
}
</style>

</head>
<body>
	<center>
		<h1>USER DATA AVAILABLE</h1>

	</center>
	<br>
	<br>
	<div class="ae">
		<div class="container">
			<table class="table table-striped table-dark">
				<thead>
					<tr>
						<th scope="col">USER ID</th>
						<th scope="col">FIRST NAME</th>
						<th scope="col">LAST NAME</th>
						<th scope="col">EMAIL</th>
						<th scope="col">PASSWORD</th>
						<th scope="col">PHONE</th>
						<th scope="col">COUNTRY</th>
					</tr>
				</thead>
				<c:forEach var="user" items="${user}">
					<tbody>
						<tr>
							<td>${user.id}</td>
							<td>${user.firstname}</td>
							<td>${user.lastname}</td>
							<td>${user.email}</td>
							<td>${user.password}</td>
							<td>${user.phone}</td>
							<td>${user.country}</td>

						</tr>
					</tbody>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>