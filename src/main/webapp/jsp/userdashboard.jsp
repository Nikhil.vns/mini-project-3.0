<%@page import="com.graded.main.model.User"%>
<%@page import="java.util.List"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<title>USER DASHBOARD</title>
<link rel="stylesheet" type="text/css" href="slide navbar style.css">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link
	href="https://fonts.googleapis.com/css2?family=Jost:wght@500&display=swap"
	rel="stylesheet">
<script src='https://kit.fontawesome.com/a076d05399.js'
	crossorigin='anonymous'></script>
<style type="text/css">
body {
	align-items: center;
	font-family: 'Jost', sans-serif;
	background: linear-gradient(to bottom, #0f0c29, #302b63, #24243e);
}

.main {
	width: 350px;
	height: 500px;
	background: red;
	overflow: hidden;
	background:
		url("https://doc-08-2c-docs.googleusercontent.com/docs/securesc/68c90smiglihng9534mvqmq1946dmis5/fo0picsp1nhiucmc0l25s29respgpr4j/1631524275000/03522360960922298374/03522360960922298374/1Sx0jhdpEpnNIydS4rnN4kHSJtU1EyWka?e=view&authuser=0&nonce=gcrocepgbb17m&user=03522360960922298374&hash=tfhgbs86ka6divo3llbvp93mg4csvb38")
		no-repeat center/cover;
	border-radius: 10px;
	box-shadow: 5px 20px 50px #000;
}

#h1 {
	color: white
}

p {
	color: white;
}

a {
	color: white;
}

.ae {
	color: white;
}

.col-4 {
	margin-bottom: 5%;
	padding-left: 5%;
}

h1 {
	color: white;
}

.bg-dark {
	background-color: #272548 !important;
}

.btn-outline-info {
	color: #f8f9fa;
	background-color: transparent;
	background-image: none;
	border-color: #f8f9fa;
}
</style>

</head>
<body>
	<form action="userdashboard" method="get">
		<center>
			<h1>Customer Relationship Management</h1>
			<br>
			<%
			String email = (String) session.getAttribute("email");
			%>
			<h2 id="h1">
				Welcome :<%=email%>
			</h2>
			<button class="btn btn-outline-light">
				<a style="text-decoration: none;" class="text-blue" href="adduser">Add
					User</a>
			</button>
			<button class="btn btn-outline-light">
				<a style="text-decoration: none;" class="text-blue" href="logout">logout</a>
			</button>
		</center>
		<br> <br>
		<div class="ae">
			<div class="container">
				<table class="table table-striped table-dark">
					<thead>
						<tr>
							<th scope="col">USER ID</th>
							<th scope="col">FIRST NAME</th>
							<th scope="col">LAST NAME</th>
							<th scope="col">EMAIL</th>
							<th scope="col">PASSWORD</th>
							<th scope="col">PHONE</th>
							<th scope="col">COUNTRY</th>
							<th scope="col">UPDATE</th>
							<th scope="col">DELETE</th>
						</tr>
					</thead>
					<c:forEach var="user" items="${contacts}">
						<tbody>
							<tr>
								<td>${user.id}</td>
								<td>${user.firstname}</td>
								<td>${user.lastname}</td>
								<td>${user.email}</td>
								<td>${user.password}</td>
								<td>${user.phone}</td>
								<td>${user.country}</td>
								<td><a href="edituser?id=${user.id}"><button
											type="button" style='font-size: 24px'>
											<i class='fas fa-cut'></i>
										</button></a></td>
								<td><a href="deleteuser?id=${user.id}"><button
											type="button" style="font-size: 24px">
											<i class="fa fa-bomb"></i>
										</button></a></td>
							</tr>
						</tbody>
					</c:forEach>
				</table>
			</div>
		</div>
</body>
</html>