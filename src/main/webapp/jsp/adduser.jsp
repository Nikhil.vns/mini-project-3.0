<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add USER</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style type="text/css">
body {
	background-image: url("Background.jpg");
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}

body {
	margin: 0;
	padding: 0;
	display: flex;
	justify-content: center;
	align-items: center;
	min-height: 100vh;
	font-family: 'Jost', sans-serif;
	background: linear-gradient(to bottom, #0f0c29, #302b63, #24243e);
}

.main {
	width: 350px;
	height: 800px;
	background: red;
	overflow: hidden;
	background:
		url("https://doc-08-2c-docs.googleusercontent.com/docs/securesc/68c90smiglihng9534mvqmq1946dmis5/fo0picsp1nhiucmc0l25s29respgpr4j/1631524275000/03522360960922298374/03522360960922298374/1Sx0jhdpEpnNIydS4rnN4kHSJtU1EyWka?e=view&authuser=0&nonce=gcrocepgbb17m&user=03522360960922298374&hash=tfhgbs86ka6divo3llbvp93mg4csvb38")
		no-repeat center/cover;
	border-radius: 10px;
	box-shadow: 5px 20px 50px #000;
}

#chk {
	display: none;
}

.signup {
	position: relative;
	width: 100%;
	height: 100%;
}

label {
	color: #fff;
	font-size: 2.3em;
	justify-content: center;
	display: flex;
	margin: 60px;
	font-weight: bold;
	cursor: pointer;
	transition: .5s ease-in-out;
}

input {
	width: 60%;
	height: 20px;
	background: #e0dede;
	justify-content: center;
	display: flex;
	margin: 20px auto;
	padding: 10px;
	border: none;
	outline: none;
	border-radius: 5px;
}

button {
	width: 60%;
	height: 40px;
	margin: 10px auto;
	justify-content: center;
	display: block;
	color: #fff;
	background: #573b8a;
	font-size: 1em;
	font-weight: bold;
	margin-top: 20px;
	outline: none;
	border: none;
	border-radius: 5px;
	transition: .2s ease-in;
	cursor: pointer;
}

button:hover {
	background: #6d44b8;
}

.login {
	height: 460px;
	background: #eee;
	border-radius: 60%/10%;
	transform: translateY(-180px);
	transition: .8s ease-in-out;
}

.login label {
	color: #573b8a;
	transform: scale(.6);
}

#chk:checked ~ .login {
	transform: translateY(-500px);
}

#chk:checked ~ .login label {
	transform: scale(1);
}

#chk:checked ~ .signup label {
	transform: scale(.6);
}

#lables {
	color: white;
	font-size: 20px;
	font-weight: bold;
	margin-left: 10%;
}

#lab {
	color: #fff;
	font-size: 2.3em;
	justify-content: center;
	display: flex;
	margin: 60px;
	font-weight: bold;
	cursor: pointer;
	transition: .5s ease-in-out;
}

.bg-info {
	background-color: #56147800 !important;
}

.bg-dark {
	background-color: #272548 !important;
}

.btn-outline-info {
	color: #f8f9fa;
	background-color: transparent;
	background-image: none;
	border-color: #f8f9fa;
}

input {
	width: 60%;
	height: 40px;
}

h3.text-center {
	color: white;
}

label {
	margin: 10px;
}
</style>
</head>
<body>
	<!-- main container  -->
	<div class='container pt-5 pb-5'>
		<div class="row">
			<div class="col-lg-3"></div>
			<div class="col-lg-6 col-lg-offset-3">
				<form action="adduser" method="post"
					class="bg-info pt-5 pb-5 pl-5 pr-5 border border-dark rounded">

					<h4 class='text-center text-danger'>${error}</h4>

					<h3 class='text-center'>Enter Details</h3>

					<div class="form-group">
						<label for="username"><h4>First Name</h4></label> <input
							type="text" class="form-control" id="email"
							placeholder="Enter your First Name" name="firstname">
					</div>

					<div class="form-group">
						<label for="username"><h4>Last Name</h4></label> <input
							type="text" class="form-control" id="email"
							placeholder="Enter your Last Name" name="lastname">
					</div>
					<div class="form-group">
						<label for="username"><h4>Email</h4></label> <input type="text"
							class="form-control" id="email" placeholder="Enter email"
							name="email">
					</div>

					<div class="form-group">
						<label for="email"><h4>Password</h4></label> <input type="text"
							class="form-control" value="" placeholder="Enter password"
							name="password">
					</div>

					<div class="form-group">
						<label for="email"><h4>Phone</h4></label> <input type="text"
							class="form-control" value="" placeholder="Enter phone"
							name="phone">
					</div>

					<div class="form-group">
						<label for="email"><h4>Country</h4></label> <input type="text"
							class="form-control" value="" placeholder="Enter country"
							name="country">
					</div>

					<div align="center">
						<button type="submit" class="btn btn-outline-light" name="adduser"
							value="edit">Add User</button>
					</div>
				</form>
			</div>
			<div class="col-lg-3"></div>
		</div>
	</div>

</body>
</html>