<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>INDEX</title>
<link rel="stylesheet" type="text/css" href="slide navbar style.css">
<link
	href="https://fonts.googleapis.com/css2?family=Jost:wght@500&display=swap"
	rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
	integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
	integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
	crossorigin="anonymous"></script>
<style type="text/css">
body {
	align-items: center;
	font-family: 'Jost', sans-serif;
	background: linear-gradient(to bottom, #0f0c29, #302b63, #24243e);
}

.main {
	width: 350px;
	height: 500px;
	background: red;
	overflow: hidden;
	background:
		url("https://doc-08-2c-docs.googleusercontent.com/docs/securesc/68c90smiglihng9534mvqmq1946dmis5/fo0picsp1nhiucmc0l25s29respgpr4j/1631524275000/03522360960922298374/03522360960922298374/1Sx0jhdpEpnNIydS4rnN4kHSJtU1EyWka?e=view&authuser=0&nonce=gcrocepgbb17m&user=03522360960922298374&hash=tfhgbs86ka6divo3llbvp93mg4csvb38")
		no-repeat center/cover;
	border-radius: 10px;
	box-shadow: 5px 20px 50px #000;
}

#h1 {
	color: white
}

p {
	color: white;
}

a {
	color: white;
}

.ae {
	color: white;
}

.col-4 {
	margin-bottom: 5%;
	padding-left: 5%;
}

h1 {
	color: white;
}

h5 {
	color: white;
}

.ae {
	padding-left: 90%;
}

marquee {
	color: white;
}
</style>

</head>
<body>
	<br>
	<div class="row container-fluid">
		<div class="col">
			<h1>CustomerRelationshipManagement</h1>
			<h5>Mini Project</h5>
		</div>
		<div class="col" style="text-align: right;">

			<div class='aes'>

				<a href='register'><button type="button"
						class="btn btn-outline-light">Register Here</button></a>
			</div>


			<p class='aes'>welcome a new user</p>
		</div>
		<div class="col-1" style="text-align: right;">
			<div class='aes'>
				<a href='login'><button type="button"
						class="btn btn-outline-light">Login</button></a>
			</div>


			<p class='aes'>Registered user</p>
		</div>
	</div>

	<div class='container'>
		<center>
			<div class='aes'>
				<a href='userlist'><button type="button"
						class="btn btn-outline-light">AVAILABLE USER</button></a></a>
			</div>


			<p class='aes'>click here to visit the LIST OF USER</p>
		</center>
		<marquee width="100%" direction="left" height="50px"> Hey I
			am Deepak ,This is my Site on Customer Relationship Management. </marquee>

	</div>
	<div id="carouselExampleIndicators" class="carousel slide"
		data-bs-ride="carousel">
		<div class="carousel-indicators">
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="0" class="active" aria-current="true"
				aria-label="Slide 1"></button>
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="1" aria-label="Slide 2"></button>
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="2" aria-label="Slide 3"></button>
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="3" aria-label="Slide 4"></button>
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="4" aria-label="Slide 5"></button>
		</div>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img
					src="https://previews.123rf.com/images/davidoff205020/davidoff2050202101/davidoff205020210100272/162165304-abstract-digital-hexadecimal-code-on-a-screen-divided-into-same-size-squares-with-changing-data-anim.jpg"
					width="30" height="400" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img
					src="https://previews.123rf.com/images/davidoff205020/davidoff2050202101/davidoff205020210100252/162027140-abstract-digital-hexadecimal-white-code-on-a-black-screen-divided-into-same-size-squares-with-changi.jpg"
					width="30" height="400" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img
					src="https://previews.123rf.com/images/gonin/gonin1710/gonin171000088/87874751-orange-matrix-rain-of-digital-hex-code-computer-generated-abstract-technology-concept.jpg"
					width="30" height="400" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img
					src="https://previews.123rf.com/images/gonin/gonin1803/gonin180300127/98101289-yellow-hex-digital-symbols-on-computer-monitor-futuristic-big-data-information-technology-concept-3d.jpg"
					width="30" height="400" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img
					src="https://previews.123rf.com/images/gonin/gonin1802/gonin180200017/96144250-colorful-hexadecimal-big-data-digital-code-futuristic-information-technology-concept-rows-of-random-.jpg"
					width="30" height="400" class="d-block w-100" alt="...">
			</div>
		</div>
		<button class="carousel-control-prev" type="button"
			data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span> <span
				class="visually-hidden">Previous</span>
		</button>
		<button class="carousel-control-next" type="button"
			data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span> <span
				class="visually-hidden">Next</span>
		</button>
	</div>
</body>
</html>